import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _ from 'lodash';

// Components
import Navbar from "../components/navbar";
import Searchbox from "../components/searchbox";
import ArtistList from "../components/artistList";
import Shortlist from "../components/shortList"

// Actions
import {fetchArtists} from "../actions";
import {addToShortlist} from "../actions";
import {clearShortlist} from "../actions";

class App extends Component {
    render() {
        const {artists} = this.props;
        const fetchArtists = _.debounce((input) => { this.props.fetchArtists(input) }, 300);
        return (
            <div>
                <Navbar
                    siteName="MusicBrainz"
                />
                <div className="container">
                    <h1>Search Last.fm</h1>
                    <Searchbox
                        onInputChange={(event) => {
                            fetchArtists(event.target.value);
                        }}
                    />
                    <br/>
                    <ArtistList
                        artists={artists}
                        shortlistAdd={this.props.addToShortlist}
                    />
                    <Shortlist
                        shortlist={this.props.shortlist}
                        clearShortlist={this.props.clearShortlist}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({artists, shortlist}) => ({artists, shortlist});
export default connect(mapStateToProps, {fetchArtists, addToShortlist, clearShortlist})(App)