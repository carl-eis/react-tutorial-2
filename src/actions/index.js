import axios from 'axios';

export const ARTIST_SEARCH = "artist_search ";
export const ARTIST_SHORTLISTED = "artist_shortlisted";
export const SHORTLIST_CLEARED = "shortlist_cleared";

export const fetchArtists = (searchText) => async (dispatch) => {
    const API_URL = `http://ws.audioscrobbler.com/2.0/`;

    try {
        const options = {
            params: {
                method: "artist.search",
                artist: searchText,
                api_key: "5077f7792dc55d7d39aedf5bf9de24da",
                format: "json"
            }
        };

        let result = await axios.get(API_URL, options);
        console.log(result);
        let artists = result.data.results.artistmatches.artist;

        dispatch({type: ARTIST_SEARCH, data: artists});

    } catch (ex) {
        console.log("fetch artists failed! ", ex);
    }
};

export const addToShortlist = (artist) => {
    return { type: ARTIST_SHORTLISTED, data: artist }
};

export const clearShortlist = () => {
    return { type: SHORTLIST_CLEARED, data: null }
};