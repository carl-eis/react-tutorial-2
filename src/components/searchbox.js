import React, {Component} from 'react';
import fontawesome from 'font-awesome/css/font-awesome.min.css'

export default class Searchbox extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="input-group">
                <input  type="text" className="form-control"
                        onChange={this.props.onInputChange}
                        placeholder={"Enter search terms here"}
                />
                <div className="input-group-btn">
                    <button className="btn btn-default">
                        <i className="fa fa-search" />
                    </button>
                </div>
            </div>
        )
    }
}