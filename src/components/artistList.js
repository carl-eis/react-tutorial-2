import React from 'react';

const ArtistList = ({artists, shortlistAdd}) => {

    if (!artists) {
        return <div/>
    }

    const artistResults = artists.map(artist => {
        return (
            <tr key={artist.name} className="fit">
                <td>
                    <img src={artist.image[2]['#text'] || "http://via.placeholder.com/120"}
                         alt=""
                        className="img-artist-profile"
                    />
                </td>
                <td>
                    {artist.name}
                </td>
                <td>
                    <button onClick={() => shortlistAdd(artist)} className="btn btn-default btn-success">Add to shortlist</button>
                </td>
            </tr>
        )
    });

    return (
        <div className="table-artists">
            <table className="table table-striped table-responsive">
                <thead>
                <tr className="fit">
                    <th className="col-md-2">
                        {/*Leave empty*/}
                    </th>
                    <th className="col-md-9">
                        <strong>Artist Name</strong>
                    </th>
                    <th className="col-md-1">

                    </th>
                </tr>
                </thead>
                <tbody>
                {artistResults}
                </tbody>
            </table>
        </div>
    );
};

export default ArtistList;