import React, {Component} from 'react';

const Navbar = (props) => {
    const pageLinks = [{
        name: "Last.fm",
        url: "/home",
        key: "homeItem"
    },{
        name: "Favourites",
        url: "/favourites",
        key: "favouritesItem"
    }];
    const navItems = pageLinks.map(link => <li key={link.key}><a href={link.url}>{link.name}</a></li>);

    return (
        <nav className="navbar navbar-inverse">
            <div className="container">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed"
                            data-toggle="collapse"
                            data-target="#menu-collapse"
                            aria-expanded="false">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <a className="navbar-brand" href="#">MusicBrainz</a>
                </div>

                <div className="collapse navbar-collapse" id="menu-collapse">
                    <ul className="nav navbar-nav">
                        {navItems}
                    </ul>
                </div>
            </div>
        </nav>
    )
};

export default Navbar;