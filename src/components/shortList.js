import React, {Component} from 'react';

const Shortlist = ({shortlist, clearShortlist}) => {
    if (shortlist.length === 0) return <div />;

    const renderListItems = (shortlist) => shortlist.map(artist => {
        return (
            <li key={artist.mbid} className="list-group-item">{artist.name}</li>
        )
    });

    return (
        <div className="shortlist">
            <h4>Shortlist</h4>

            <button className="btn btn-danger btn-clear-shortlist" onClick={() => clearShortlist()}>
                Clear Shortlist
            </button>

            <ul className="list-group">
                {renderListItems(shortlist)}
            </ul>
        </div>
    )
};

export default Shortlist;