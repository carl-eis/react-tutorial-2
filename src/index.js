import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// Dependencies
import jQuery from 'jquery/src/jquery'; // Required by bootstrap!!!
import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Components
import App from './containers/app.js'
import Navbar from "./components/navbar";
import Searchbox from "./components/searchbox";
import SearchResults from "./components/artistList";

// Redux
import reducers from './reducers';

// const createStoreWithMiddleware = applyMiddleware()(createStore, thunk);

const store = createStore(
    reducers,
    {},
    compose(
        applyMiddleware(thunk)
    )
);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root'));
