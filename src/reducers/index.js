import {combineReducers} from 'redux';
import artists from './reducer_artists'
import shortlist from './reducer_shortlist'

const rootReducer = combineReducers({artists, shortlist});

export default rootReducer;