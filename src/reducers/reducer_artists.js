import {ARTIST_SEARCH} from "../actions/index";

export default function (state = null, action) {
    switch (action.type) {
        case ARTIST_SEARCH:
            return action.data;
        default:
            return state;
    }
}