import _ from 'lodash';
import {ARTIST_SHORTLISTED} from "../actions/index";
import {SHORTLIST_CLEARED} from "../actions/index";

export default function (state = [], action) {
    switch (action.type) {
        case ARTIST_SHORTLISTED:
            return _.uniqBy([action.data, ...state], "mbid");
        case SHORTLIST_CLEARED:
            return [];

        default:
            return state;
    }
}