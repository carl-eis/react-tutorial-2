# LastFM Artist Search

### Preamble

This repo is built with yarn. If you don't have it, please

    $ npm install -g yarn

before continuing.

### Getting Started

- Clone this repo
- Run `yarn` inside the project directory
- Run `yarn start` and visit `localhost:3000` in your browser!

### Some notes

- I am aware of the race condition on the searchbar
(will fix later if necessary)
- I am aware of the API key commited to the repo! Usually I have a secrets file,
but this key is disposable so I'm not going to bother.